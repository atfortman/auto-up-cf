# Auto Update CloudFlare

This tool compares the public IP address of a server, or whatever device it's running on, to the publicly resolving DNS record. If the two are different, the zone file is updated to match the public IP address.